<?php
namespace Core\Model;

use Core\Registry\Registry;

class Model
{
    private $_id;
    private $_db;
    private $_data;
    
    private $table;
    private $column;
    private $writeable;
    
    public function toArray()
    {
        return $this->_data;
    }
    
    //override
    public static function getTable()
    {
        return basename(str_replace('\\', '/', get_called_class())) . 's';
    }
    //override
    public static function getColumn()
    {
        return 'id';
    }
    
    //TODO: tidy this
    public function __construct($id = 0, $table = '', $column = '', $include_deleted = false, $data=array())
    {
        $this->_id = $id;
        $this->_db = Registry::get('db');
        $this->table = empty($table) ? static::getTable() : $table;
        $this->column = empty($column) ? static::getColumn() : $column;
        
        if($id || count($data) > 0 )
        {
            $data = count($data) > 0 ? $data : $this->_db->selectRow($this->table, $this->column.'=:id', array('id' => $id));   
            if(count($data) > 0)
            {
                $this->_data = $data;
            }
            else
            {
                $this->_data = null;
            }
                      
            $this->_id = $this->_data['id'];
            if(!$include_deleted && $this->get('deleted') != null)
            {
                if($this->get('deleted'))
                {
                    $this->_data = null;
                    $this->_id = 0;
                }
            }
        }
    }
    public static function fetchMany($ids, $include_deleted = false)
    {
        $db = Registry::get('db');
        $prepared_array = array();
        $keys = array();
        foreach($ids as $key => $id)
        {
            $prepared_array[':id'.$key] = $id;
            $keys[] = ':id'.$key;
        }
        $rows = $db->select(static::getTable(), static::getColumn().' IN ('.implode(',', $keys).')', $prepared_array);
        $result = array();
        foreach($rows as $row)
        {
            $id = $row[static::getColumn()];
            $model = new static($id, '', '', $include_deleted, $row);
            if($model->toArray() != null)
                (yield $id => $model);
        }
    }
    public function setValues($keyvalue)
    {
        foreach($keyvalue as $key => $value)
        {
            if(!isset($this->writeable) || in_array($key, $this->writeable))
            {
                $this->_data[$key] = $value;
            }
        }
        return $this;
    }
    public function getValues()
    {
        return $this->_data;
    }
    public function set($column, $value)
    {
        if(!isset($this->writeable) || in_array($column, $this->writeable))
        {
            $this->_data[$column] = $value;
        }
        return $this;
    }
    public function get($column)
    {
        if(isset($this->_data[$column]))
        {
            return $this->_data[$column];
        }
        else
        {
            return null;
        }
    }
    public function save()
    {
        if($this->_id == 0)
        {
            return $this->_db->insert($this->table, $this->_data);
        }
        else
        {
            return $this->_db->update($this->table, $this->_data, self::getColumn().'=:id', array('id' => $this->_id));
        }
    }
    public function delete($hard = false)
    {
        if($this->get('deleted') != null)
        {
            $this->set('deleted', 1);
        }
        if($this->get('deleted_date') != null)
        {
            $this->set('deleted_date', time());
        }
        if($hard)
        {
            $this->_db->delete($this->table, 'id=:id', array('id'=>$this->_id));
        }
        $this->save();
    }
    public static function create()
    {
        return new static();
    }
    public static function fetch($id = 0, $table = '', $column = '', $include_deleted = false, $data=array())
    {
        return new static($id, $table, $column, $include_deleted, $data);
    }
}
