# CoreFramework
Simple MVC framework

Learning a new framework shouldn't be like relearning PHP again.

The goal of Core is to be easy to pickup while still offering all the features you need and also being fast and secure.